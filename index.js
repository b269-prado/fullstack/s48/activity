// S48 Reactive DOM with Fetch
// console.log("hello world");

// Fetch
fetch('https://jsonplaceholder.typicode.com/posts/')
.then((response) => response.json())
// .then((json) => console.log(json));
.then((data) => showPosts(data));

// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	// Prevents the page from reloading
	// "preventDefault()" - prevents default behavior of an event
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/', {
		method: 'POST', 
		headers: { 'Content-Type': 'application/json; charset=UTF-8'}, 
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully added!');

		// Resets the input fields
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});
});


// Retrieve post
const showPosts = (posts) => {

	// "postEntries" is a variable that will contain all the posts
	let postEntries = "";

	posts.forEach((post) => {

		postEntries +=`
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};


// Edit post
const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	document.querySelector("#btn-submit-update").removeAttribute('disabled');
};


// Update post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: { 'Content-Type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully updated')

		// Reset the edit post form input fields
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector("#btn-submit-update").setAttribute('disabled', true);
	});
});


// Delete Post
const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, { method: 'DELETE' });

	// The Element.remove() method removes the element from the DOM.
	document.querySelector(`#post-${id}`).remove();
};

